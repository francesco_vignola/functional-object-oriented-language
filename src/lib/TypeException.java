package lib;

public class TypeException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String text;

	public TypeException (String t) {
		 FOOLlib.typeErrors++;
		 text=t;
    }
		  
	
}
