package lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import ast.ArrowTypeNode;
import ast.BoolTypeNode;
import ast.EmptyTypeNode;
import ast.IntTypeNode;
import ast.Node;
import ast.RefTypeNode;

public class FOOLlib {

	public static int typeErrors = 0;

	public static final int MEMESIZE = 10000;
	
	public static List<List<String>> dispatchTables = new ArrayList<>();
	
	private static Map<String, String> superTypes = new HashMap<>();
	
	// valuta se il tipo "a" � <= al tipo "b", dove "a" e "b" sono tipi di base: int o bool
	public static boolean isSubtype(Node a, Node b) {
		return a.getClass().equals(b.getClass()) ||
				(a instanceof BoolTypeNode && b instanceof IntTypeNode) ||
				isArrowTypeNode(a, b) ||
				isSubTypeIfNull(a, b) ||
				isSuperType(b, a);
	}
	
	/*
	 * Entrambi i nodi devono essere ArrowTypeNode
	 */
	private static boolean isArrowTypeNode(Node a, Node b) {
		return a.getClass().equals(ArrowTypeNode.class) &&
				b.getClass().equals(ArrowTypeNode.class) &&
				isCovariant(a, b) &&
				isControvariant(a, b);
	}
	
	/*
	 * Perch� la controvarianza sia soddisfatta:
	 * - il tipo di ritorno del nodo padre deve essere
	 *   dello stesso tipo o super-tipo di quello del nodo figlio.
	 */
	private static Boolean isCovariant(Node a, Node b) {
		Node aReturnType = ((ArrowTypeNode) a).getReturnType();
		Node bReturnType = ((ArrowTypeNode) b).getReturnType();
		
		return isSuperType(aReturnType, bReturnType);
	}

	/*
	 * Perch� la controvarianza sia soddisfatta:
	 * - i due nodi devono avere lo stesso numero di parametri
	 * - ogni parametro della funzione, in modo ordinato,
	 *   del nodo figlio deve essere dello stresso tipo o
	 *   super-tipo del parametro corrispondente nel nodo padre. 
	 */	
	private static Boolean isControvariant(Node a, Node b) {
		List<Node> aParameters = ((ArrowTypeNode) a).getParList();
		List<Node> bParameters = ((ArrowTypeNode) b).getParList();
		
		return aParameters.size() == bParameters.size() &&
				IntStream.range(0, aParameters.size() - 1)
						.allMatch(ith -> isSuperType(bParameters.get(ith), aParameters.get(ith)));
	}
	
	private static boolean isSuperType(Node a, Node b) {
		boolean ret = false;
		
		if (a instanceof RefTypeNode && b instanceof RefTypeNode) {
			RefTypeNode father = (RefTypeNode) a;
			RefTypeNode child = (RefTypeNode) b;
			
			String idFather = father.getId();
			String idChild = child.getId();
			
			ret = isSuperType(idFather, idChild);
		} 
		
		return ret;
	}
	
	private static boolean isSuperType(String idSuper, String id) {
		boolean ret = false;
		
		if (superTypes.containsKey(id)) {
			String idChild = superTypes.get(id);
			String idFather = superTypes.get(idSuper);
			
			ret = idChild == idSuper ? true : isSuperType(idFather, id);
		}
		
		return ret;
	}
	
	private static boolean isSubTypeIfNull(Node a, Node b) {
		return a instanceof EmptyTypeNode && b instanceof RefTypeNode;
	}
	
	public static Node lowestCommonAncestor(Node a, Node b) {
		Node ancestor = null;
		
		if (a instanceof RefTypeNode && b instanceof RefTypeNode) {
			RefTypeNode aReference = (RefTypeNode) a;
			RefTypeNode bReference = (RefTypeNode) b;
			ancestor = getCommonAncestor(aReference.getId(), bReference.getId());
		} else if (a instanceof EmptyTypeNode) {
			ancestor = b;
		} else if (b instanceof EmptyTypeNode) {
			ancestor = a;
		} else if (a instanceof IntTypeNode || b instanceof IntTypeNode) {
			ancestor = new IntTypeNode();
		} else if (a instanceof BoolTypeNode && b instanceof BoolTypeNode) {
			ancestor = new BoolTypeNode();
		} else if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			ArrowTypeNode aFunctional = (ArrowTypeNode) a;
			ArrowTypeNode bFunctional = (ArrowTypeNode) b;
			
			List<Node> aFunctionaParameters = aFunctional.getParList();
			List<Node> bFunctionaParameters = bFunctional.getParList();
			
			if (aFunctionaParameters.size() == bFunctionaParameters.size()) {
				Node commonAncestor = lowestCommonAncestor(aFunctional.getReturnType(), bFunctional.getReturnType());
				List<Node> controvariantTypes = controvariantTypes(aFunctionaParameters, bFunctionaParameters);
				
				if (commonAncestor != null && aFunctionaParameters.size() == controvariantTypes.size()) {
					ancestor = new ArrowTypeNode(controvariantTypes, commonAncestor);
				}
			}
		}
		
		return ancestor;
	}
	
	private static List<Node> controvariantTypes(List<Node> aParameters, List<Node> bParameters) {
		List<Node> types = new ArrayList<>();
		for(int index = 0; index < aParameters.size(); index++) {
			Node aParameter = aParameters.get(index);
			Node bParameter = bParameters.get(index);
			
			if (isSubtype(aParameter, bParameter)) {
				types.add(aParameter);
			} else if (isSubtype(bParameter, aParameter)) {
				types.add(bParameter);
			}
		}
		return types;
	}
	
	private static RefTypeNode getCommonAncestor(String a, String b) {
		RefTypeNode aNode = new RefTypeNode(a);
		
		if (isSubtype(new RefTypeNode(b), aNode)) {
			return aNode;
		}
		
		return superTypes.get(a) != null ? getCommonAncestor(superTypes.get(a), b) : null;
	}

	private static int labCount = 0;

	public static String freshLabel() {
		return "label" + (labCount++);
	}

	private static int funlabCount = 0;

	public static String freshFunLabel() {
		return "function" + (funlabCount++);
	}

	private static int metLabCount = 0;

	public static String freshMetLabel() {
		return "method" + (metLabCount++);
	}
	
	private static String funCode = "";

	public static void putCode(String c) {
		funCode += "\n" + c; // aggiunge una linea vuota di separazione prima di funzione
	}

	public static String getCode() {
		return funCode;
	}

	public static void addSuperType(String subType, String superType) {
		superTypes.put(subType, superType);		
	}

	/*	Without inheritance
	 	
	 	private static Boolean isCovariant(Node a, Node b) {
			Class<? extends Node> aReturnType = ((ArrowTypeNode) a).getRet().getClass();
			Class<? extends Node> bReturnType = ((ArrowTypeNode) b).getRet().getClass();
			return aReturnType.isAssignableFrom(bReturnType);
		}
	
		private static Boolean isControvariant(Node a, Node b) {
			List<Node> aParameters = ((ArrowTypeNode) a).getParList();
			List<Node> bParameters = ((ArrowTypeNode) b).getParList();
			return aParameters.size() == bParameters.size() && IntStream.range(0, aParameters.size() - 1)
				.allMatch(ith -> bParameters.get(ith).getClass().isAssignableFrom(aParameters.get(ith).getClass()));
		}
 */
}
