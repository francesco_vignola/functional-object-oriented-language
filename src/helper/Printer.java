package helper;
import java.util.List;

import ast.Node;

public class Printer {
	public static String makePrintable(List<? extends Node> nodes, String indent) {
		return nodes
				.stream()
				.map(node -> node.toPrint(indent + "  "))
				.reduce("", (partial, printableNode) -> partial + printableNode);
	}
}
