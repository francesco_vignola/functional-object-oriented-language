package ast;

import java.util.ArrayList;
import java.util.List;

import helper.Printer;
import lib.CloneException;
import lib.FOOLlib;
import lib.TypeException;

public class MethodNode extends AbstractDecNode {

	private Node body;
	private List<Node> parameters, declarations;
	private String label;

	private int offset;
	
	public MethodNode(String id, Node symType) {
		super(id, symType);
		this.parameters = new ArrayList<>();
		this.declarations = new ArrayList<>();
	}
	
	public String getId() {
		return super.id;
	}
	
	public List<Node> getParameters() {
		return parameters;
	}
	
	public int getOffset() {
		return offset;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setBody(Node body) {
		this.body = body;
	}

	public void addDeclaration(Node declaration) {
		this.declarations.add(declaration);
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public void addParameter(Node parameter) {
		this.parameters.add(parameter);
	}
	
	@Override
	public String toPrint(String indent) {
	    return indent + "Fun:" + id + "\n" +
	    		type.toPrint(indent + "  ") +
	    		Printer.makePrintable(parameters, indent) +
	    		Printer.makePrintable(declarations, indent) +
	    		body.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		for (Node dec : declarations)
			try {
				dec.typeCheck();
			} catch (TypeException e) {
				System.out.println("Type checking error in a declaration: " + e.text);
			}
		if (!FOOLlib.isSubtype(body.typeCheck(), type))
			throw new TypeException("Wrong return type for method " + id);
		return null;
	}

	@Override
	public String codeGeneration() {
		String declCode = "", popDecl = "", popParl = "";
		for (Node dec : declarations) {
			declCode += dec.codeGeneration();
			
			DecNode node = (DecNode) dec;
			
			if (node.getSymType() instanceof ArrowTypeNode) {
				popDecl += "pop\n";
			}
			popDecl += "pop\n";
		}
		
		for (Node par : parameters) {
			DecNode node = (DecNode) par;
			if (node.getSymType() instanceof ArrowTypeNode)
				popParl += "pop\n";
			popParl += "pop\n";
		}
		
		label = FOOLlib.freshMetLabel();
		
		String code = label + ":\n" +
				"cfp\n" + // set $fp to $sp value
				"lra\n" +
				declCode +
				body.codeGeneration() + // push $ra value
															// generate code for local declarations (they use the new $fp!!!)
				 											// generate code for function body expression
				"stm\n" +
				popDecl + // set $tm to popped value (function result)
									// remove local declarations from stack
				"sra\n" + // set $ra to popped value
				"pop\n" + // remove Access Link from stack
				popParl + // remove parameters from stack
				"sfp\n" + // set $fp to popped value (Control Link)
				"ltm\n" + // push $tm value (function result)
				"lra\n" +
				"js\n" // jump to $ra value
				;
		
		FOOLlib.putCode(code);
		return "";
	}
	
	@Override
	public MethodNode cloneNode() throws CloneException {
		MethodNode node = new MethodNode(this.id, this.type.cloneNode());
		node.setBody(body);
		node.setOffset(offset);
//		node.setLabel(label);
		
		for (Node parameter : this.parameters) {
			node.addParameter(parameter.cloneNode());
		}
		
		for (Node declaration : this.declarations) {
			node.addDeclaration(declaration.cloneNode());
		}
		
		return node;
	}

}
