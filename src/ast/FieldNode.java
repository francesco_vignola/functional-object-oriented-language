package ast;

import lib.CloneException;
import lib.TypeException;

public class FieldNode extends AbstractDecNode {

	private int offset;
	
	public FieldNode(String id, Node symType) {
		super(id, symType);
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	@Override
	public String toPrint(String indent) {
		 return indent + "Field:" + super.id + "\n" +
				 	type.toPrint(indent + "  "); 
	}

	@Override
	public Node typeCheck() throws TypeException {
		/* Not use */
		return null;
	}

	@Override
	public String codeGeneration() {
		return "";
	}

	@Override
	public FieldNode cloneNode() throws CloneException {
		return new FieldNode(this.id, this.type.cloneNode());
	}

}
