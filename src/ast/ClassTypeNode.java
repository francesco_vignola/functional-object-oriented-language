package ast;

import java.util.ArrayList;
import java.util.List;

import helper.Printer;
import lib.CloneException;
import lib.TypeException;

public class ClassTypeNode implements Node {
	
	private final List<FieldNode> fields;
	private final List<MethodNode> methods;
	
	public ClassTypeNode() {
		this(new ArrayList<>(), new ArrayList<>());
	}

	public ClassTypeNode(List<FieldNode> fields, List<MethodNode> methods) {
		super();
		this.fields = fields;
		this.methods = methods;
	}

	public List<FieldNode> getFields() {
		return fields;
	}
	
	public List<MethodNode> getMethods() {
		return this.methods;
	}
	
	public void addField(FieldNode field, int offset) {
		this.fields.add(offset, field);
	}
	
	public void addOverrideField(FieldNode field, int offset) {
		this.fields.set(offset, field);
	}
	
	public void addMethod(MethodNode method, int offset) {
		this.methods.add(offset, method);
	}

	@Override
	public String toPrint(String indent) {
	    return indent + "ClassTypeNode\n" +
	    		Printer.makePrintable(fields, indent) +
	    		Printer.makePrintable(methods, indent);
	}

	@Override
	public Node typeCheck() throws TypeException {
		/* Not used */
		return null;
	}

	@Override
	public String codeGeneration() {
		return "";
	}

	@Override
	public Node cloneNode() throws CloneException {
		List<FieldNode> fields = new ArrayList<>();
		for (FieldNode parameter : this.fields) {
			fields.add(parameter.cloneNode());
		}
		
		List<MethodNode> methods = new ArrayList<>();
		for (MethodNode method : this.methods) {
			methods.add(method.cloneNode());
		}
		
		return new ClassTypeNode(fields, methods);
	}

	public void addOverrideMethod(MethodNode meth, int superOffset) {
		this.methods.set(superOffset, meth);
	}

}
