package ast;

import java.util.ArrayList;
import java.util.List;

import helper.Printer;
import lib.FOOLlib;
import lib.TypeException;

public class ClassNode extends AbstractDecNode {

	private String superId = "";
	private STentry superEntry;
	private List<FieldNode> fields;
	private List<MethodNode> methods;
	
	public ClassNode(String id, Node type) {
		super(id, type);
		this.fields = new ArrayList<>();
		this.methods = new ArrayList<>();
	}

	public ClassNode(String id, Node type, List<FieldNode> fields, List<MethodNode> methods) {
		this(id, type);
		this.fields.addAll(fields);
		this.methods.addAll(methods);
	}
	
	public void addField(FieldNode field, int offset) {
		this.fields.add(offset, field);
	}
	
	public void addOverrideField(FieldNode field, int offset) {
		this.fields.set(offset, field);
	}
	
	public void addMethod(MethodNode method, int offset) {
		this.methods.add(offset, method);
	}
	
	public void addOverrideMethod(MethodNode meth, int superOffset) {
		this.methods.set(superOffset, meth);
	}

	public void setSuperId(String superId) {
		this.superId = superId;
	}
	
	public void setSuperEntry(STentry superEntry) {
		this.superEntry = superEntry;
	}

	@Override
	public String toPrint(String indent) {
		String printableExtends = superId.isEmpty() ? superId : "extends" + superId;
	    return indent + "Class " + super.id + printableExtends + "\n" +
	    		Printer.makePrintable(fields, indent) +
	    		Printer.makePrintable(methods, indent);
	}

	@Override
	public Node typeCheck() throws TypeException {
		if (superEntry != null) {
			ClassTypeNode superType = (ClassTypeNode) superEntry.getType();
			List<FieldNode> superTypeFields = superType.getFields();
			List<MethodNode> superTypeMethods = superType.getMethods();
			
			for (int index = 0; index < fields.size(); index++) {
				int offset = -fields.get(index).getOffset() - 1; // Ottimizzazione 
				
				if (offset < superTypeFields.size()) {	// Ottimizzazione
					// Anche senza ottimizzazione
					Node subTypeField = this.fields.get(index);
					if (!FOOLlib.isSubtype(subTypeField, superTypeFields.get(index))) {
						throw new TypeException("Type checking error in a field: " + (index + 1) + "-th field isn't subtype of the " + (index + 1) + "-th field of the inherited class");
					}
				}
			}

			for (int index = 0; index < methods.size(); index++) {
				int offset = methods.get(index).getOffset(); // Ottimizzazione 
				
				if (offset < superTypeMethods.size()) {	// Ottimizzazione
					Node subTypeMethod = this.methods.get(index);
					if (FOOLlib.isSubtype(subTypeMethod, superTypeMethods.get(index))) {
						subTypeMethod.typeCheck();
					} else {
						throw new TypeException("Type checking error in a method: " + (index + 1) + "-th return type isn't subtype of the " + (index + 1) + "-th return type of the inherited class");
					}
				}
			}
		} else {
			for (Node node : this.methods) {
				node.typeCheck();
			}
		}
		
		return null;
	}

	@Override
	public String codeGeneration() {
		
		final List<String> dispatchTable = superEntry != null ?
				FOOLlib.dispatchTables.get(-superEntry.getOffset() - 2) :
				new ArrayList<>();
		
		methods.stream().forEachOrdered(method -> {
			method.codeGeneration();
			dispatchTable.add(method.getOffset(), method.getLabel());
		});
		
		FOOLlib.dispatchTables.add(dispatchTable);
		
		return dispatchTable.stream()
				.map(label -> "push " + label + "\n" +
								"lhp\n" +
								"sw\n" +
								"lhp\n" +
								"push 1\n" +
								"add\n" +
								"shp\n")
				.reduce("lhp\n", (partial, methodString) -> partial + methodString);
	}

}
