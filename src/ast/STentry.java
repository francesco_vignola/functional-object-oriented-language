package ast;

import lib.CloneException;

public class STentry {

	private Node type;
	private int nestingLevel, offset;
	private boolean isMethod;

	private STentry(int nestingLevel, int offset, boolean isMethod) {
		super();
		this.nestingLevel = nestingLevel;
		this.offset = offset;
		this.isMethod = isMethod;
	}

	public Node getType() {
		return type;
	}

	public int getOffset() {
		return offset;
	}

	public int getNestingLevel() {
		return nestingLevel;
	}

	public void setType(Node type) {
		this.type = type;
	}

	public String toPrint(String indent) {
		return indent + "STentry: nestlev " + nestingLevel + "\n" +
				indent + "STentry: type\n " + type.toPrint(indent + "  ") +
				indent + "STentry: offset " + offset + "\n" +
				indent + "isMethod " + isMethod + "\n";
	}
	
	@Override
	public String toString() {
		return this.nestingLevel + ", " + type.toString();
	}
	
	public static STentry getMethodSTentry(int nestingLevel, int offset) {
		return new STentry(nestingLevel, offset, true);
	}
	
	public static STentry getSTentry(int nestingLevel, int offset) {
		return new STentry(nestingLevel, offset, false);
	}

	public boolean isMethod() {
		return this.isMethod;
	}

	public STentry cloneNode() throws CloneException {
		STentry entry = new STentry(this.nestingLevel, this.offset, this.isMethod);
		entry.setType(type.cloneNode());
		return entry;
	}

}