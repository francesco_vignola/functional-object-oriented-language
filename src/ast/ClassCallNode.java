package ast;

import java.util.ArrayList;
import java.util.List;

import helper.Printer;
import lib.FOOLlib;
import lib.TypeException;

public class ClassCallNode implements Node {

	private final String idObjectClass, idClassMethod;
	private final STentry classEntry, methodEntry;
	private final List<Node> arguments;
	private final int nestingLevel;

	public ClassCallNode(
			String idObjectClass, String idClassMethod,
			STentry classEntry, STentry methodEntry,
			List<Node> arguments,
			int nestingLevel) {
		this.idObjectClass = idObjectClass;
		this.idClassMethod = idClassMethod;
		this.classEntry = classEntry;
		this.methodEntry = methodEntry;
		this.arguments = arguments;
		this.nestingLevel = nestingLevel;
	}

	@Override
	public String toPrint(String indent) {
	    return indent + "ClassCall: " + idObjectClass + "." + idClassMethod +
	    		" at nestinglevel " + nestingLevel + "\n" +  
			    classEntry.toPrint(indent + "  ") +
			    methodEntry.toPrint(indent + "  ") + 
			    Printer.makePrintable(arguments, indent);
	}

	@Override
	public Node typeCheck() throws TypeException {
		ArrowTypeNode t = (ArrowTypeNode) methodEntry.getType();
		List<Node> p = t.getParList();
		
		if (!(p.size() == arguments.size()))
			throw new TypeException("Wrong number of parameters in the invocation of " + idClassMethod);
		
		for (int i = 0; i < arguments.size(); i++)
			if (!(FOOLlib.isSubtype((arguments.get(i)).typeCheck(), p.get(i))))
				throw new TypeException("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + idClassMethod);
		
		return t.getReturnType();
	}

	@Override
	public String codeGeneration() {
		String parCode = "", getAR = "";
		for (int i = arguments.size() - 1; i >= 0; i--) {
			parCode += arguments.get(i).codeGeneration();
		}
		
		for (int i = 0; i < nestingLevel - classEntry.getNestingLevel(); i++) {
			getAR += "lw\n";
		}
	
		return "lfp\n" +								// push Control Link (pointer to frame of function id caller)
				parCode +								// generate code for parameter expressions in reversed order
				code(getAR, classEntry.getOffset()) +
				"stm\n" + "ltm\n" + "ltm\n" +			// duplicate top of the stack
				"lw\n" +
				"lw\n" +
				"push " + methodEntry.getOffset() + "\n" +
				"add\n" +
				"lw\n" +
				"js\n";		// l'indirizzo dell'AR che contiene la dichiarazione di id;
	}
	
	private String code(String ar, int offset) {
		// risalgo la catena statica degli AL per ottenere
		return "push " + offset + "\n" + // l'indirizzo dell'AR che contiene la dichiarazione di id
				"lfp\n" +
				ar +
				"add\n" +
				"lw\n";
	}

}
