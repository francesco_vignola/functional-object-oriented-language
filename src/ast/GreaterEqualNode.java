package ast;

import lib.CloneException;
import lib.FOOLlib;
import lib.TypeException;

public class GreaterEqualNode implements Node {

	private Node left, right;

	public GreaterEqualNode(Node left, Node right) {
		super();
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "GreaterEqual\n" + left.toPrint(indent + "  ") + right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		Node l = left.typeCheck();
		Node r = right.typeCheck();
		if (!(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l)))
			throw new TypeException("Incompatible types in greater or equal");
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {

		String l1 = FOOLlib.freshLabel();
		String l2 = FOOLlib.freshLabel();
		String l3 = FOOLlib.freshLabel();
		return left.codeGeneration() +
				right.codeGeneration() +
				"bleq " + l1 + "\n" +
				"b " + l2 + "\n" +
				l1+ ": \n" +
				left.codeGeneration() +
				right.codeGeneration() +
				"beq " + l3 + "\n" +
				"push 0\n" +
				"b " + l2 + "\n" +
				l3 + ": \n" +
				"push 1\n" +
				l2 + ": \n";
	}

	@Override
	public Node cloneNode() throws CloneException {
		return new GreaterEqualNode(left.cloneNode(), right.cloneNode());
	}

}
