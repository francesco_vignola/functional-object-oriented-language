package ast;

import lib.CloneException;
import lib.TypeException;

public interface Node {

	String toPrint(String indent);

	// fa il type checking e ritorna:
	// per una espressione, il suo tipo (oggetto BoolTypeNode o IntTypeNode)
	// per una dichiarazione, "null"
	Node typeCheck() throws TypeException;

	String codeGeneration();
	
	default Node cloneNode() throws CloneException {
		throw new CloneException("Clone non supportata per la classe: " + this.getClass());
	}

}