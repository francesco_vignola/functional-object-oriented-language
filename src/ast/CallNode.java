package ast;

import java.util.ArrayList;
import java.util.List;

import helper.Printer;
import lib.FOOLlib;
import lib.TypeException;

public class CallNode implements Node {

	private String id;
	private STentry entry;
	private int nestingLevel;
	private ArrayList<Node> parlist = new ArrayList<Node>();

	public CallNode(String id, STentry entry, ArrayList<Node> parList, int nestingLevel) {
		this.id = id;
		this.entry = entry;
		this.parlist = parList;
		this.nestingLevel = nestingLevel;
	}

	public String toPrint(String indent) {
		return indent + "Call:" + id + " at nestinglevel " + nestingLevel + "\n" + entry.toPrint(indent + "  ")
				+ Printer.makePrintable(parlist, indent);
	}

	/*
	 * Il tipo dell'id deve essere funzionale: nome di funzione oppure parametro o variabile.
	 */
	public Node typeCheck() throws TypeException {
		if (!(entry.getType() instanceof ArrowTypeNode)) {
			throw new TypeException("In CallNode" + " Invocation of a non-function " + id);
		}

		ArrowTypeNode t = (ArrowTypeNode) entry.getType();
		List<Node> p = t.getParList();
		if (!(p.size() == parlist.size())) {
			throw new TypeException("In CallNode" + " Wrong number of parameters in the invocation of " + id);
		}

		for (int i = 0; i < parlist.size(); i++) {

			if (!(FOOLlib.isSubtype((parlist.get(i)).typeCheck(), p.get(i)))) {
				throw new TypeException("In CallNode" + " Wrong type for " + (i + 1) + "-th parameter in the invocation of " + id);
			}
		}
		return t.getReturnType();
	}

	public String codeGeneration() {
		String parCode = "", getAR = "";

		for (int i = parlist.size() - 1; i >= 0; i--) {
			parCode += parlist.get(i).codeGeneration();
		}

		for (int i = 0; i < nestingLevel - entry.getNestingLevel(); i++) {
			getAR += "lw\n";

			if (entry.isMethod()) { // se � metodo => ritorno codice di estensione Object Oriented
				getAR += "lw\n"; 	// aggiungo un salto in pi�
			}
		}

		int offset = entry.getOffset();

		return "lfp\n" 		// push Control Link (pointer to frame of function id caller)
				+ parCode 	// generate code for parameter expressions in reversed order
				+ getCode(offset, getAR) + getCode(offset - 1, getAR) + "js\n"; // jump to popped address (putting in $ra address of subsequent instruction)
	}

	public String getCode(int offset, String activationRecordAddress) {
		return "push " + offset + "\n" +
				"lfp\n" +
				activationRecordAddress + 	// push Access Link (pointer to frame of function id declaration, reached as for variable id)
				"add\n" +
				"lw\n"; 					// push function address (value at: pointer to frame of function id declaration + its offset)
	}

}