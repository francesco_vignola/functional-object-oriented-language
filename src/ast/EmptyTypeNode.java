package ast;

import lib.CloneException;
import lib.TypeException;

public class EmptyTypeNode implements Node {

	@Override
	public String toPrint(String indent) {
		return indent + "EmptyType\n";
	}

	@Override
	public Node typeCheck() throws TypeException {
		return null;
	}

	@Override
	public String codeGeneration() {
		return "";
	}

	@Override
	public Node cloneNode() throws CloneException {
		return new EmptyTypeNode();
	}

}
