package ast;

import lib.CloneException;
import lib.FOOLlib;
import lib.TypeException;

public class OrNode implements Node {

	private Node left, right;

	public OrNode(Node left, Node right) {
		super();
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Or\n" + left.toPrint(indent + "  ") + right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		if (!(FOOLlib.isSubtype(left.typeCheck(), new BoolTypeNode())
				&& FOOLlib.isSubtype(right.typeCheck(), new BoolTypeNode())))
			throw new TypeException("Non boolean expressions in or");
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel();
		String l2 = FOOLlib.freshLabel();
		String l3 = FOOLlib.freshLabel();
		return left.codeGeneration() + right.codeGeneration() + "beq " + l1 + "\n" + "push 1\n" + "b " + l2 + "\n" + l1
				+ ": \n" + left.codeGeneration() + "push 0\n" + "beq " + l3 + "\n" + "push 1\n" + "b " + l2 + "\n" + l3
				+ ": \n" + "push 0\n" + l2 + ": \n";
	}

	@Override
	public Node cloneNode() throws CloneException {
		return new OrNode(left.cloneNode(), right.cloneNode());
	}

}
