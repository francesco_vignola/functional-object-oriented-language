package ast;

import java.util.ArrayList;

import helper.Printer;
import lib.FOOLlib;
import lib.TypeException;

public class ProgLetInNode implements Node {

	private ArrayList<Node> declist;
	private Node exp;

	public ProgLetInNode(ArrayList<Node> d, Node e) {
		declist = d;
		exp = e;
	}

	public String toPrint(String indent) {
		return indent + "ProgLetIn\n" +
				Printer.makePrintable(declist, indent) +
				exp.toPrint(indent + "  ");
	}

	public Node typeCheck() throws TypeException {
		for (Node dec : declist)
			try {
				dec.typeCheck();
			} catch (TypeException e) {
				System.out.println("Type checking error in a declaration: " + e.text);
			}
		return exp.typeCheck();
	}

	public String codeGeneration() {
		String declCode = "";
		
		for (Node dec : declist)
			declCode += dec.codeGeneration();
		return "push 0\n" + declCode + exp.codeGeneration() + "halt\n" + FOOLlib.getCode();
	}
}