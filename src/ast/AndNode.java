package ast;

import lib.CloneException;
import lib.FOOLlib;
import lib.TypeException;

public class AndNode implements Node {

	private Node left, right;

	public AndNode(Node left, Node right) {
		super();
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "And\n" + left.toPrint(indent + "  ") + right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		if (!(isBool(left) && isBool(right)))
			throw new TypeException("Non boolean expressions in and");
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel();
		String l2 = FOOLlib.freshLabel();
		String l3 = FOOLlib.freshLabel();
		return left.codeGeneration() +
				"push 0\n" +
				"beq " + l1 + "\n" +
				"push 1\n" +
				right.codeGeneration() +
				"beq " + l3 + "\n" +
				"push 0\n" +
				"b " + l2 + "\n" +
				l1 + ": \n" +
				"push 0\n" +
				"b " + l2 + "\n" +
				l3 + ": \n" +
				"push 1\n" +
				l2 + ": \n";
	}

	private Boolean isBool(Node node) throws TypeException {
		return FOOLlib.isSubtype(node.typeCheck(), new BoolTypeNode());
	}

	@Override
	public Node cloneNode() throws CloneException {
		return new AndNode(left.cloneNode(), right.cloneNode());
	}

}
