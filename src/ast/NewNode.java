package ast;

import java.util.ArrayList;
import java.util.List;

import helper.Printer;
import lib.CloneException;
import lib.FOOLlib;
import lib.TypeException;

public class NewNode implements Node {

	private final STentry entry;
	private final String idClassName;
	private final List<Node> arguments;
	
	public NewNode(String idClassName, STentry entry, List<Node> arguments) {
		this.entry = entry;
		this.arguments = arguments;
		this.idClassName = idClassName;
	}
	
	@Override
	public String toPrint(String indent) {
	    return indent + "New: " + idClassName + "\n" +
	    		Printer.makePrintable(arguments, indent);
	}

	@Override
	public Node typeCheck() throws TypeException {
		ClassTypeNode classTypeNode = (ClassTypeNode) entry.getType();
		List<FieldNode> fields = classTypeNode.getFields();
		
		if (fields.size() != arguments.size()) {
			throw new TypeException("Wrong number of parameters in the invocation of " + idClassName);
		}
		
		for (int i = 0; i < arguments.size(); i++) {
			if (!(FOOLlib.isSubtype((arguments.get(i)).typeCheck(), fields.get(i).getSymType()))) {
				throw new TypeException("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + idClassName);
			}
		}
		
		return new RefTypeNode(idClassName);
	}

	@Override
	public String codeGeneration() {
		String code="";

		//la codeGeneration viene richiamata su tutti i parametri della new () in ordine di apparizione (che
		//mettono ciascuno il loro valore calcolato (potrei dover calcolare un'espressione var a:A = new A(3,4,5)) sullo stack)
		for (int i=arguments.size()-1; i>=0; i--) {

			code+=
					//PRENDO I VALORI DEI PARAMETRI, uno alla volta, dallo stack e li mette nello heap, dopo ogni parametro incremento hp
					arguments.get(i).codeGeneration() + //carico codice del parametro sullo stack		
					"lhp\n" + //carico codice dell'heap pointer sullo stack	
					"sw\n" + //salto all'indirizzo dell'heap pointer (e poppo ind. dallo stack), poi memorizzo nella pos di hp il valore del param.
					"lhp\n" + //INCREMENTO HP DI 1 //pusho hp sullo stack
					"push 1\n" + // 
					"add\n" + // sommo il valore del rigistro hp con 1
					"shp\n";				
		}

		//metto il dispatch pointer  sullo heap
		return  code + 
				"push " + FOOLlib.MEMESIZE + "\n" +
				"push " + entry.getOffset() + "\n" + //salvo il valore che ho appena incrementato 
				"add\n" + //sommo: memsize + offset e ottengo dispatch pointer 
				"lhp\n" +//carico codice dell'heap pointer sullo stack	
				"sw\n" + //metto il dispatch pointer (che ho sullo stack) nello heap all'indirizzo specificato da lhp
		//carico sullo stack l'OBJECT POINTER
				"lhp\n" +
		//incremento l'heap pointer e lo salvo in hp
				"lhp\n" +
				"push 1\n" +
				"add\n" +
				"shp\n"
				;
		/*
		for (int i = arguments.size() - 1; i >= 0; i--)
			arguments.get(i).codeGeneration();
		
		String code = arguments.stream()
				.map(argument -> "sw\n" + incrementHeapPointer() + "shp\n")
				.reduce("lhp\n", (partial, argumentString) -> partial + argumentString);
		code += "push " + FOOLlib.MEMESIZE + "\n" + "push " + entry.getOffset() + "\n" + "add\n" + "lhp\n" + "sw\n";
		code += "lhp\n" + incrementHeapPointer() + "shp\n";
		
		return code;
		*/
	}
	
	private String incrementHeapPointer() {
		return "lhp\n" + "push 1\n" + "add\n";
	}

	@Override
	public Node cloneNode() throws CloneException {
		List<Node> args = new ArrayList<>();
		
		for (Node parameter : this.arguments) {
			args.add(parameter.cloneNode());
		}
		
		return new NewNode(idClassName, this.entry.cloneNode(), arguments);
	}

}
