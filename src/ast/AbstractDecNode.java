package ast;

public abstract class AbstractDecNode implements DecNode {

	protected String id;
	protected Node type;

	public AbstractDecNode(String id, Node symType) {
		super();
		this.id = id;
		this.type = symType;
	}

	public Node getSymType() {
		return type;
	}

}
