package ast;

public interface DecNode extends Node {
	
	/*
	 * Ritorna il tipo, messo in Symbol Table,
	 * di un nodo (VarNode, FunNode, ParNode).
	 */
	public Node getSymType();
}
