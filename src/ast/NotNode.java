package ast;

import lib.CloneException;
import lib.FOOLlib;
import lib.TypeException;

public class NotNode implements Node {

	private Node exp;

	public NotNode(Node exp) {
		super();
		this.exp = exp;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Not\n" + exp.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		if (!FOOLlib.isSubtype(exp.typeCheck(), new BoolTypeNode()))
			throw new TypeException("Non boolean value in not");
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel();
		String l2 = FOOLlib.freshLabel();
		return exp.codeGeneration() + "push 0\n" + "beq " + l1 + "\n" + "push 0\n" + "b " + l2 + "\n" + l1 + ": \n"
				+ "push 1\n" + l2 + ": \n";
	}

	@Override
	public Node cloneNode() throws CloneException {
		return new NotNode(this.exp.cloneNode());
	}

}
