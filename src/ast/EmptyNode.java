package ast;

import lib.CloneException;
import lib.TypeException;

public class EmptyNode implements Node {

	@Override
	public String toPrint(String indent) {
		return indent + "Null\n";
	}

	@Override
	public Node typeCheck() throws TypeException {
		return new EmptyTypeNode();
	}

	@Override
	public String codeGeneration() {
		return "push -1\n";
	}

	@Override
	public Node cloneNode() throws CloneException {
		return new EmptyNode();
	}

}
