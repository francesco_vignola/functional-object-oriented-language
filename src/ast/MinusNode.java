package ast;

import lib.CloneException;
import lib.FOOLlib;
import lib.TypeException;

public class MinusNode implements Node {

	private Node left, right;

	public MinusNode(Node left, Node right) {
		super();
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Minus\n" + left.toPrint(indent + "  ") + right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		if (!(FOOLlib.isSubtype(left.typeCheck(), new IntTypeNode())
				&& FOOLlib.isSubtype(right.typeCheck(), new IntTypeNode())))
			throw new TypeException("Non integers in difference");
		return new IntTypeNode();
	}

	@Override
	public String codeGeneration() {
		return left.codeGeneration() + right.codeGeneration() + "sub\n";
	}

	@Override
	public Node cloneNode() throws CloneException {
		return new MinusNode(left.cloneNode(), right.cloneNode());
	}

}
