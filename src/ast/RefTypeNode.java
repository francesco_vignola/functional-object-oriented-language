package ast;

import lib.CloneException;
import lib.TypeException;

public class RefTypeNode implements Node {

	private final String idClassName;
	
	public RefTypeNode(String idClassName) {
		super();
		this.idClassName = idClassName;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "RefType: " + this.idClassName + "\n";
	}

	@Override
	public Node typeCheck() throws TypeException {
		return null;
	}

	@Override
	public String codeGeneration() {
		return "";
	}

	public String getId() {
		return this.idClassName;
	}

	@Override
	public Node cloneNode() throws CloneException {
		return new RefTypeNode(this.idClassName);
	}

}
