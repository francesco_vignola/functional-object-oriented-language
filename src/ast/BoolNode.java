package ast;

import lib.CloneException;

public class BoolNode implements Node {

	private boolean val;

	public BoolNode(boolean n) {
		val = n;
	}

	public String toPrint(String indent) {
		return indent + "Bool: " + val + "\n";
	}

	public Node typeCheck() {
		return new BoolTypeNode();
	}

	public String codeGeneration() {
		return "push " + (val ? 1 : 0) + "\n";
	}

	@Override
	public Node cloneNode() throws CloneException {
		return new BoolNode(val);
	}
	
}