package ast;

import lib.CloneException;

public class ParNode extends AbstractDecNode {

	public ParNode(String i, Node t) {
		super(i, t);
	}

	public String getId() {
		return id;
	}
	
	public String toPrint(String s) {
		return s + "Par:" + id + "\n" + type.toPrint(s + "  ");
	}

	// non utilizzato
	public Node typeCheck() {
		return null;
	}

	// non utilizzato
	public String codeGeneration() {
		return "";
	}

	@Override
	public Node cloneNode() throws CloneException {
		return new ParNode(this.id, this.type.cloneNode());
	}

}