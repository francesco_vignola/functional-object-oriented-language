package ast;

import lib.CloneException;
import lib.TypeException;

public class IdNode implements Node {

	private String id;
	private int nestingLevel;
	private STentry entry;

	public IdNode(String i, STentry st, int nl) {
		id = i;
		nestingLevel = nl;
		entry = st;
	}

	public String toPrint(String s) {
		return s + "Id:" + id + " at nestinglevel " + nestingLevel + "\n" + entry.toPrint(s + "  ");
	}

	/*
	 * Per Higher Order ammettiamo anche il tipo funzionale.
	 */
	public Node typeCheck() throws TypeException {
		if (entry.getType() instanceof ClassTypeNode || entry.isMethod()) {
			throw new TypeException("Wrong usage of function identifier " + id);
		}
		
		return entry.getType();
	}

	public String codeGeneration() {
		String getAR = "";
		for (int i = 0; i < nestingLevel - entry.getNestingLevel(); i++)
			getAR += "lw\n";
		
		return code(getAR, entry.getOffset()) + 
				(entry.getType() instanceof ArrowTypeNode ? 
						code(getAR, entry.getOffset() - 1) : "");
	}
	
	private String code(String ar, int offset) {
		return "lfp\n" + ar + // risalgo la catena statica degli AL per ottenere
				"push " + offset + "\n" +
				"add\n" +
				"lw\n"; // l'indirizzo dell'AR che contiene la dichiarazione di id
	}

	@Override
	public Node cloneNode() throws CloneException {
		return new IdNode(this.id, this.entry.cloneNode(), this.nestingLevel);
	}
	
	/* Senza Higher Order n� OO
		public Node typeCheck() throws TypeException {
		 	if (entry.getType() instanceof ArrowTypeNode)
		 		throw new TypeException("Wrong usage of function identifier "+id);
			 return entry.getType();
		}
	*/
	
}