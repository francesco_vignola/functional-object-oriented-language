package ast;

import java.util.ArrayList;
import java.util.List;

import helper.Printer;
import lib.CloneException;

public class ArrowTypeNode implements Node {

	private List<Node> parlist;
	private Node ret;

	public ArrowTypeNode(List<Node> controvariantTypes, Node r) {
		parlist = controvariantTypes;
		ret = r;
	}

	public Node getReturnType() {
		return ret;
	}

	public List<Node> getParList() {
		return parlist;
	}

	public String toPrint(String indent) {
		return indent + "ArrowTypeNode\n" +
				Printer.makePrintable(parlist, indent) +
				ret.toPrint(indent + "  ->");
	}

	// non utilizzato
	public Node typeCheck() {
		return null;
	}

	// non utilizzato
	public String codeGeneration() {
		return "";
	}

	@Override
	public Node cloneNode() throws CloneException {
		ArrayList<Node> parameters = new ArrayList<>();
		for (Node parameter : parlist) {
			parameters.add(parameter.cloneNode());
		}
		return new ArrowTypeNode(parameters, ret.cloneNode());
	}

}