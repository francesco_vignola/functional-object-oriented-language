package ast;

import java.util.ArrayList;

import helper.Printer;
import lib.FOOLlib;
import lib.TypeException;

public class FunNode extends AbstractDecNode {

	private ArrayList<Node> parlist = new ArrayList<Node>(); // campo "parlist"
																// che � lista
																// di Node
	private ArrayList<Node> declist = new ArrayList<Node>();
	private Node exp, symType;

	public FunNode(String i, Node t) {
		super(i, t);
	}

	public Node getSymType() {
		return symType;
	}

	public void setSymType(Node symType) {
		this.symType = symType;
	}

	public void addDec(ArrayList<Node> d) {
		declist = d;
	}

	public void addBody(Node b) {
		exp = b;
	}

	public void addPar(Node p) { // metodo "addPar" che aggiunge un nodo a campo
									// "parlist"
		parlist.add(p);
	}

	public String toPrint(String indent) {
		return indent + "Fun:" + id + "\n" +
				type.toPrint(indent + "  ") +
				Printer.makePrintable(parlist, indent) +
				Printer.makePrintable(declist, indent) +
				exp.toPrint(indent + "  ");
	}

	public Node typeCheck() throws TypeException {
		for (Node dec : declist)
			try {
				dec.typeCheck();
			} catch (TypeException e) {
				System.out.println("Type checking error in a declaration: " + e.text);
			}
		if (!FOOLlib.isSubtype(exp.typeCheck(), type))
			throw new TypeException("Wrong return type for function " + id);
		return null;
	}

	public String codeGeneration() {

		String declCode = "", popDecl = "", popParl = "";
		for (Node dec : declist) {
			DecNode node = (DecNode) dec;
			declCode += dec.codeGeneration();
			if (node.getSymType() instanceof ArrowTypeNode) {
				popDecl += "pop\n";
			}
			popDecl += "pop\n";
		}
		
		for (Node par : parlist) {
			DecNode node = (DecNode) par;
			if (node.getSymType() instanceof ArrowTypeNode)
				popParl += "pop\n";
			popParl += "pop\n";
		}

		String funl = FOOLlib.freshFunLabel();
		FOOLlib.putCode(funl + ":\n" +
						"cfp\n" + // set $fp to $sp value
						"lra\n" +
						declCode +
						exp.codeGeneration() + // push $ra value
																	// generate code for local declarations (they use the new $fp!!!)
						 											// generate code for function body expression
						"stm\n" + popDecl + // set $tm to popped value (function result)
											// remove local declarations from stack
						"sra\n" + // set $ra to popped value
						"pop\n" + // remove Access Link from stack
						popParl + // remove parameters from stack
						"sfp\n" + // set $fp to popped value (Control Link)
						"ltm\n" + // push $tm value (function result)
						"lra\n" +
						"js\n" // jump to $ra value
		);
		return "lfp\n" + "push " + funl + "\n";
	}
	
}