grammar FOOL;

@header{
	import java.util.ArrayList;
	import java.util.HashMap;
	import java.util.HashSet;
	import java.util.Map;
	import java.util.Set;
	import lib.*;
	import ast.*;
}

@parser::members{
	int symbolTableErrors = 0;
	
	private int nestingLevel = 0;
	private int globalOffset = -2;
	private ArrayList<HashMap<String, STentry>> symTable = new ArrayList<>();
	private Map<String, Map<String, STentry>> classTable = new HashMap<>();
	
	//livello ambiente con dichiarazioni piu' esterno � 0 (prima posizione ArrayList) invece che 1 (slides)
	//il "fronte" della lista di tabelle � symTable.get(nestingLevel)
}

@lexer::members {
	int lexicalErrors = 0;
}

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/

prog returns [Node ast]:
	{
		/*
		 * Scope entry:
		 * - creo una nuova tabella e
		 * - la aggiungo al fronte della lista.
		 */
		HashMap<String, STentry> hm = new HashMap<> ();
		symTable.add(hm);
	}
	(
		e=exp 				{ $ast = new ProgNode($e.ast); }
		| LET 				{ ArrayList<Node> allDeclarations = new ArrayList<>(); }
			(
				c=cllist	{ allDeclarations.addAll($c.classlist); } (d=declist { allDeclarations.addAll($d.astlist); })?
				| d=declist	{ allDeclarations.addAll($d.astlist); }
			)
		IN e=exp 			{ $ast = new ProgLetInNode(allDeclarations, $e.ast); }
	)
	{ 
		/*
		 * Scope exit: rimuovo la HashMap al nesting level corrente.
		 */
		symTable.remove(nestingLevel);
	}
	SEMIC EOF
	;

cllist returns [ArrayList<Node> classlist] : { $classlist = new ArrayList<Node>(); }
	(
		CLASS idClass=ID
		{
			int methodOffset = 0;
			int fieldOffset = -1;
			Set<String> classDeclarations = new HashSet<>();
			ClassTypeNode classTypeNode = new ClassTypeNode();
			ClassNode classNode = new ClassNode($idClass.text, classTypeNode);
            HashMap<String, STentry> virtualTable = new HashMap<>();
            Map<String, STentry> superVirtualTable = new HashMap<>();
		}
		(
			EXTENDS idSuperClass=ID
			{ 
				//Risalta alla ricerca della dichiarazione
				int j = nestingLevel;
				STentry entry = null;
				
				while (j >= 0 && entry == null)
					entry = symTable.get(j--).get($idSuperClass.text);
				
				if (entry == null) {
					System.out.println("Superclass id " + $idSuperClass.text + " at line " + $idSuperClass.line + " not declared");
					symbolTableErrors++;
				} else {
					classNode.setSuperEntry(entry);
					FOOLlib.addSuperType($idClass.text, $idSuperClass.text);
				}
				
				try {
					classTypeNode = (ClassTypeNode) entry.getType().cloneNode();
					classNode = new ClassNode($idClass.text, classTypeNode, classTypeNode.getFields(), classTypeNode.getMethods());
					classNode.setSuperId($idSuperClass.text);
					
					superVirtualTable = classTable.get($idSuperClass.text);
					Set<String> keys = superVirtualTable.keySet(); 
				     
				    for (String k : keys) {
						virtualTable.put(k, superVirtualTable.get(k).cloneNode());
				    }
				} catch(CloneException exception) {
					exception.printStackTrace();
				}
				
				methodOffset = classTypeNode.getMethods().size();
				fieldOffset = -classTypeNode.getFields().size();
			}
		)?
			{
	            STentry entry = STentry.getSTentry(nestingLevel, globalOffset--);
				entry.setType(classTypeNode);
				
				HashMap<String, STentry> hm = symTable.get(nestingLevel);
				
				if ( hm.put($idClass.text, entry) != null  ) {
					System.out.println("Class id " + $idClass.text + " at line " + $idClass.line + " already declared");
                	symbolTableErrors++;
				}
	            
	            classTable.put($idClass.text, virtualTable);
	            
	            /*
				 * Scope entry:
				 * - aggiungo la Virtual table al fronte della lista
				 * - incremento il nesting level per entrare in unu nuovo scope.
				 */
	            symTable.add(virtualTable);
	            nestingLevel++;
			}
			LPAR
			(
				idField=ID COLON t=type
				{
					// Ottimizzazione: Non � necessario controllare la ridefinizione (erronea) del campo perch� il primo
					
					FieldNode field = new FieldNode($idField.text, $t.ast);
					
					/* FOOL without inheritance */
					/* 
					 	STentry fieldEntry = STentry.getSTentry(nestingLevel, fieldOffset--);
					 	fieldEntry.setType($t.ast);
					 
						if (virtualTable.put($idField.text, fieldEntry) != null ) {
							System.out.println("Field id " + $idField.text + " at line " + $idField.line + " already declared");
		                	symbolTableErrors++;
						}
		            */
					
					//aggiungo dich a virtualTable
					STentry fieldEntry = null;
					if (virtualTable.containsKey($idField.text)) {
						STentry superEntry = virtualTable.get($idField.text);
						
						if (superEntry.isMethod()) {
							System.out.println("Field id " + $idField.text + " at line " + $idField.line + " is overriding a method");
	                		symbolTableErrors++;
						} else {
							int superOffset = superEntry.getOffset();
							field.setOffset(superOffset);								/* Ottimizzazione */
							fieldEntry = STentry.getSTentry(nestingLevel, superOffset);
							int index = -superOffset-1;
							classTypeNode.addOverrideField(field, index);
							classNode.addOverrideField(field, index);	
						}
					} else {
						int index = -fieldOffset-1;
						classTypeNode.addField(field, index);
						classNode.addField(field, index);
						field.setOffset(fieldOffset);									/* Ottimizzazione */
						fieldEntry = STentry.getSTentry(nestingLevel, fieldOffset--);
					}
					
					fieldEntry.setType($t.ast);
					virtualTable.put($idField.text, fieldEntry);
				}
				(
					COMMA idFields=ID COLON ts=type
					{
						/* Ottimizzazione */
						if (classDeclarations.contains($idFields.text)) {
							System.out.println("Redefinition of field id " + $idField.text + " at line " + $idField.line);
							System.exit(0);
						}				
						/* ------------- */
						
						FieldNode fields = new FieldNode($idFields.text, $ts.ast);

						/* FOOL without inheritance */
						/* 
						 	STentry fieldsEntry = STentry.getSTentry(nestingLevel, fieldOffset--);
							
							if (virtualTable.put($idFields.text, fieldsEntry) != null ) {
								System.out.println("Field id " + $idFields.text + " at line " + $idFields.line + " already declared");
			                	symbolTableErrors++;
							}
			            */
			            
						//aggiungo dich a virtualTable
						STentry fieldsEntry = null;
						if (virtualTable.containsKey($idFields.text)) {
							STentry superEntry = virtualTable.get($idFields.text);
							
							if (superEntry.isMethod()) {
								System.out.println("Field id " + $idFields.text + " at line " + $idFields.line + " is overriding a method");
		                		symbolTableErrors++;
							} else {
								int superOffset = superEntry.getOffset();
								fields.setOffset(superOffset);								/* Ottimizzazione */
								fieldsEntry = STentry.getSTentry(nestingLevel, superOffset);
								int index = -superOffset-1;
								classTypeNode.addOverrideField(field, index);
								classNode.addOverrideField(field, index);
							}
						} else {
							int index = -fieldOffset-1;
							classTypeNode.addField(fields, index);
							classNode.addField(fields, index);
							fields.setOffset(fieldOffset);									/* Ottimizzazione */
							fieldsEntry = STentry.getSTentry(nestingLevel, fieldOffset--);
						}

						fieldsEntry.setType($ts.ast);
						virtualTable.put($idFields.text, fieldsEntry);
					}
				)*
				
			)?
			RPAR
			CLPAR
			(
				FUN idMethod=ID COLON typeMethod=type
				{
					/* Ottimizzazione */
					if (classDeclarations.contains($idMethod.text)) {
						System.out.println("Redefinition of method id " + $idMethod.text + " at line " + $idMethod.line);
						System.exit(0);
					}
					/* ------------- */
					
					ArrayList<Node> parameters = new ArrayList<>();
					
					//inserimento di ID nella symtable
					MethodNode meth = new MethodNode($idMethod.text, $typeMethod.ast);
					
					/* FOOL without inheritance */
					/*
					 	STentry methodEntry = STentry.getMethodSTentry(nestingLevel, methodOffset++);

						if (virtualTable.put($idMethod.text, methodEntry) != null ) {
							System.out.println("Method id " + $idMethod.text + " at line " + $idMethod.line + " already declared");
		                	symbolTableErrors++;
						}
		            */
					
					//aggiungo dich a virtualTable
					STentry methodEntry = null;
					if (virtualTable.containsKey($idMethod.text)) {
						STentry superEntry = virtualTable.get($idMethod.text);
						
						if (!superEntry.isMethod()) {
							System.out.println("Method id " + $idMethod.text + " at line " + $idMethod.line + " is overriding a field");
	                		symbolTableErrors++;
						} else {
							int superOffset = superEntry.getOffset();
							meth.setOffset(superOffset);
							methodEntry = STentry.getMethodSTentry(nestingLevel, superOffset);
							classTypeNode.addOverrideMethod(meth, superOffset);
							classNode.addOverrideMethod(meth, superOffset);
						}
					} else {
						meth.setOffset(methodOffset);
						classTypeNode.addMethod(meth, methodOffset);
						classNode.addMethod(meth, methodOffset);
						methodEntry = STentry.getMethodSTentry(nestingLevel, methodOffset++);
					}
					
					virtualTable.put($idMethod.text, methodEntry);
		            
		            /* FOOl with and without inheritance */
		            /*
					 * Scope entry:
					 * - creo una nuova tabella e
					 * - la aggiungo al fronte della lista
					 * - incremento il nesting level per entrare in un nuovo scope.
					 */
		            HashMap<String, STentry> hmn = new HashMap<>();
		            symTable.add(hmn);
		            nestingLevel++;
				}
				LPAR
				(
					methodParameter=ID COLON typeMethodParameter=hotype
					{
						int paroffset = 1;
						parameters.add($typeMethodParameter.ast);
						
						ParNode parameterNode = new ParNode($methodParameter.text, $typeMethodParameter.ast);
						meth.addParameter(parameterNode);
						
						//aggiungo dich a hmn
						STentry parameterEntry = STentry.getSTentry(nestingLevel, paroffset);
						
						/* 
						 * Per i tipi funzionali l'offset vale doppio:
						 * - indirizzo dell'AR della dichiarazioni della funzione
						 * - indirizzo della funzione del chiamante per saltare alla sua istruzione successiva (Return Address)
						 */
						if ($typeMethodParameter.ast instanceof ArrowTypeNode) {
							paroffset = paroffset + 2;
						}
						
						parameterEntry.setType($typeMethodParameter.ast);
						
						if ( hmn.put($methodParameter.text, parameterEntry) != null  ) {
							System.out.println("Parameter id " + $methodParameter.text + " at line " + $methodParameter.line + " already declared");
							symbolTableErrors++;
						}
					}
					(
						COMMA methodParameters=ID COLON typeMethodParameters=hotype
						{
							parameters.add($typeMethodParameters.ast);
							
							ParNode parametersNode = new ParNode($methodParameters.text, $typeMethodParameters.ast);
							meth.addParameter(parametersNode);
							
							//aggiungo dich a hmn
							STentry parametersEntry = STentry.getSTentry(nestingLevel, paroffset);
							
							/* 
							 * Per i tipi funzionali l'offset vale doppio:
							 * - indirizzo dell'AR della dichiarazioni della funzione
							 * - indirizzo della funzione del chiamante per saltare alla sua istruzione successiva (Return Address)
							 */
							if ($typeMethodParameters.ast instanceof ArrowTypeNode) {
								paroffset = paroffset + 2;	
							}
							
							parametersEntry.setType($typeMethodParameters.ast);
							
							if (hmn.put($methodParameters.text, parametersEntry) != null ) {
								System.out.println("Parameter id " + $methodParameters.text + " at line " + $methodParameters.line + " already declared");
								symbolTableErrors++;
							}
						}
					)*
				)?
				RPAR
                (
                	LET	{ int varOffset = -2; }
                	(
                		VAR varInMethod=ID COLON typeVarInMethod=type ASS e=exp SEMIC
                		{
                			VarNode variable = new VarNode($varInMethod.text, $typeVarInMethod.ast, $e.ast);
							meth.addDeclaration(variable);
							
							HashMap<String, STentry> table = symTable.get(nestingLevel);

							STentry varEntry = STentry.getSTentry(nestingLevel, varOffset--);
							varEntry.setType($typeVarInMethod.ast);
			
							if (table.put($varInMethod.text, varEntry) != null) {
								System.out.println("Var id " + $varInMethod.text + " at line " + $varInMethod.line + " already declared");
				              	symbolTableErrors++;
				         	}
                		}
                	)+
                	IN
                )?
                body=exp
                { 
                	meth.setBody($body.ast);
                	methodEntry.setType(new ArrowTypeNode(parameters, $typeMethod.ast));
					
					/*
					 * Scope exit:
					 * - rimuovo la HashMap al nesting level corrente e
					 * - decrementiamo il nesting level per ripristinarlo a quello dello scope pi� esterno rispetto a quello corrente.
					 */
					symTable.remove(nestingLevel--);	
                }
             	SEMIC
           	)*
			CRPAR
			{ 
				/*
				 * Scope exit:
				 * - rimuovo la HashMap al nesting level corrente e
				 * - decrementiamo il nesting level per ripristinarlo a quello dello scope pi� esterno rispetto a quello corrente.
				 */
				symTable.remove(nestingLevel--);
				$classlist.add(classNode);
			}
      )+
;

declist	returns [ArrayList<Node> astlist]:
	{
		$astlist = new ArrayList<Node>();
		int offset = nestingLevel == 0 ? globalOffset : -2;
	}
	(
		(
			VAR i=ID COLON t=hotype ASS e=exp
			{
				VarNode v = new VarNode($i.text, $t.ast, $e.ast);
				$astlist.add(v);
				HashMap<String, STentry> hm = symTable.get(nestingLevel);
	
				STentry varEntry = STentry.getSTentry(nestingLevel, offset);
				varEntry.setType($t.ast);

				if (hm.put($i.text, varEntry) != null ) {
					System.out.println("Var id "+$i.text+" at line "+$i.line+" already declared");
	              	symbolTableErrors++;
	         	}
				offset = v.getSymType() instanceof ArrowTypeNode ? offset - 2 : offset - 1;
	        }  
	      	| FUN i=ID COLON t1=type
			{
				//inserimento di ID nella symtable
				FunNode f = new FunNode($i.text, $t1.ast);
				$astlist.add(f);
				HashMap<String, STentry> hm = symTable.get(nestingLevel);
				ArrayList<Node> parTypes = new ArrayList<Node>();
				
				STentry entry = STentry.getSTentry(nestingLevel, offset);
				
				/* 
				 * Per i tipi funzionali l'offset vale doppio:
				 * - indirizzo dell'AR della dichiarazioni della funzione
				 * - indirizzo della funzione del chiamante per saltare alla sua istruzione successiva (Return Address)
				 */ 
				offset = offset - 2;
				
				if ( hm.put($i.text, entry) != null  ) {
					System.out.println("Fun id "+$i.text+" at line "+$i.line+" already declared");
                	symbolTableErrors++;
				}
				
			 	/*
				 * Scope entry:
				 * - creo una nuova tabella e
				 * - la aggiungo al fronte della lista
				 * - incremento il nesting level per entrare in un nuovo scope.
				 */
	            HashMap<String, STentry> hmn = new HashMap<>();
	            symTable.add(hmn);
	            nestingLevel++;
            }
			LPAR { int paroffset=1; }
			(
				fid=ID COLON fty=hotype
				{
					parTypes.add($fty.ast);
					
					//creo nodo ParNode
					ParNode fpar = new ParNode($fid.text, $fty.ast);
					
					//lo attacco al FunNode con addPar
					f.addPar(fpar);
					

					//aggiungo dich a hmn
					STentry parameterEntry = STentry.getSTentry(nestingLevel, paroffset);
					
					/* 
					 * Per i tipi funzionali l'offset vale doppio:
					 * - indirizzo dell'AR della dichiarazioni della funzione
					 * - indirizzo della funzione del chiamante per saltare alla sua istruzione successiva (Return Address)
					 */ 
					if ($fty.ast instanceof ArrowTypeNode) {
						paroffset =  paroffset + 2;
					}
					
					parameterEntry.setType($fty.ast);
					
					if ( hmn.put($fid.text, parameterEntry) != null  ) {
						System.out.println("Parameter id "+$fid.text+" at line "+$fid.line+" already declared");
						symbolTableErrors++;
					}
				}
				(
					COMMA id=ID COLON ty=hotype
					{
						parTypes.add($ty.ast);
						ParNode par = new ParNode($id.text, $ty.ast);
						f.addPar(par);
						paroffset = $ty.ast instanceof ArrowTypeNode ? paroffset + 1 : paroffset;
						
						STentry parametersEntry = STentry.getSTentry(nestingLevel, paroffset);
						
						/* 
						 * Per i tipi funzionali l'offset vale doppio:
						 * - indirizzo dell'AR della dichiarazioni della funzione
						 * - indirizzo della funzione del chiamante per saltare alla sua istruzione successiva (Return Address)
						 */ 
						if ($ty.ast instanceof ArrowTypeNode) {
							paroffset =  paroffset + 2;
						}
						
						parametersEntry.setType($ty.ast);
						
						if ( hmn.put($id.text, parametersEntry) != null  ) {
							System.out.println("Parameter id "+$id.text+" at line "+$id.line+" already declared");
							symbolTableErrors++;
						}
					}
				)*
			)?
			RPAR
			{
				ArrowTypeNode type = new ArrowTypeNode(parTypes, $t1.ast);
				entry.setType(type);
				f.setSymType(type);
			}
			(
				LET d=declist IN { f.addDec($d.astlist); }
			)? e=exp
			{
				f.addBody($e.ast);
				
				/*
				 * Scope exit:
				 * - rimuovo la HashMap al nesting level corrente e
				 * - decrementiamo il nesting level per ripristinarlo a quello dello scope pi� esterno rispetto a quello corrente.
				 */
				symTable.remove(nestingLevel--);
			}
		) SEMIC
    )+          
	;
	
type returns [Node ast]:
	INT		{ $ast = new IntTypeNode(); }
	| BOOL	{ $ast = new BoolTypeNode(); }
	| idClassName=ID	{ $ast =  new RefTypeNode($idClassName.text); }
	;

exp	returns [Node ast]:
	f=term { $ast = $f.ast; }
	(
		PLUS l=term		{ $ast = new PlusNode($ast, $l.ast); }
		| MINUS l=term	{ $ast = new MinusNode($ast, $l.ast); }
		| OR l=term		{ $ast = new OrNode($ast, $l.ast); }
	)*
 	;
 	
term returns [Node ast]:
	f=factor { $ast = $f.ast; }
	(
		TIMES l=factor { $ast = new TimesNode($ast, $l.ast); }
		| DIV l=factor { $ast = new DivNode($ast, $l.ast); }
		| AND l=factor { $ast = new AndNode($ast, $l.ast); }
	)*
	;
	
factor returns [Node ast]:
	f=value { $ast = $f.ast; }
	(
		EQ l=value			{ $ast = new EqualNode($ast, $l.ast); }
		| GEQ l=value		{ $ast = new GreaterEqualNode($ast, $l.ast); }
		| LEQ	 l=value	{ $ast = new LessEqualNode($ast, $l.ast); }
	)*
 	;	 	
 
value returns [Node ast]:
	n=INTEGER   		{ $ast = new IntNode(Integer.parseInt($n.text)); }
	| TRUE 				{ $ast = new BoolNode(true); }
	| FALSE				{ $ast = new BoolNode(false); }
	| NULL      		{ $ast = new EmptyNode(); }
	| NEW idClassName=ID
		{
			STentry entry = symTable.get(0).get($idClassName.text);
			if (!classTable.containsKey($idClassName.text) || entry == null) {
				System.out.println("Class Id " + $idClassName.text + " at line " + $idClassName.line + " not declared");
				symbolTableErrors++;
			}
		}
		LPAR { List<Node> arguments = new ArrayList<>(); }
			(
				argument=exp			{ arguments.add($argument.ast); }
				(COMMA arguments=exp	{ arguments.add($arguments.ast); })*
			)?
		RPAR 							{ $ast = new NewNode($idClassName.text, entry, arguments); }
	| LPAR e=exp RPAR					{ $ast = $e.ast; }
	| IF x=exp THEN CLPAR y=exp CRPAR 
		   ELSE CLPAR z=exp CRPAR 		{ $ast = new IfNode($x.ast, $y.ast, $z.ast); }
	| NOT LPAR e=exp 					{ $ast = new NotNode($e.ast); } RPAR
	| PRINT LPAR e=exp RPAR				{ $ast = new PrintNode($e.ast); }
	| i=ID
	{
		//cercare la dichiarazione
		int j = nestingLevel;
		STentry entry = null;
		
		while (j >= 0 && entry == null)
			entry = (symTable.get(j--)).get($i.text);
		
		if (entry == null) {
			System.out.println("Id " + $i.text + " at line " + $i.line + " not declared");
			symbolTableErrors++;
		}

		$ast = new IdNode($i.text, entry, nestingLevel);
	}
	(
		LPAR { ArrayList<Node> arglist = new ArrayList<Node>(); }
		(
			a=exp { arglist.add($a.ast); }
			(COMMA a=exp { arglist.add($a.ast); })*
		)?
		RPAR { $ast = new CallNode($i.text, entry, arglist, nestingLevel); }
		| DOT id2=ID LPAR { List<Node> arguments = new ArrayList<>(); }
		(
			a=exp { arguments.add($a.ast); } 
			(COMMA as=exp { arguments.add($as.ast); })*
		)?
		{
			/* TODO Controllo se id � riferimento di una classe */
			
			RefTypeNode typeObject = (RefTypeNode) entry.getType();
			String idObjectClass = typeObject.getId();
			
			Map<String, STentry> virtualTable = classTable.get(idObjectClass);
			
			STentry methodEntry = virtualTable.get($id2.text);
			
			if (methodEntry == null) {
				System.out.println("Method Id " + $id2.text + " at line " + $id2.line + " not declared");
				symbolTableErrors++;
			}
			
			$ast = new ClassCallNode(idObjectClass, $id2.text, entry, methodEntry, arguments, nestingLevel);
		}
		RPAR
	)?
	; 

// Oltre ai tipi di base (non-terminale "type")
// anche i tipi funzionali (non-terminale "arrow")
hotype returns [Node ast]:
	t=type 		{ $ast = $t.ast; }
	| a=arrow 	{ $ast = $a.ast; }
	;

// Tipi funzionali per eventuali parametri
// Tipi  di base per il tipo di ritorno
arrow returns [Node ast]:
	LPAR { ArrayList l = new ArrayList<Node>(); }
		(
			h=hotype { l.add($h.ast); }
			(COMMA h1=hotype { l.add($h1.ast); })*
		)?
	RPAR
	ARROW t=type { $ast = new ArrowTypeNode(l, $t.ast); };   


/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

COLON	: ':' ;
COMMA	: ',' ;
ASS	    : '=' ;
SEMIC   : ';' ;
EQ      : '==' ;
PLUS	: '+' ;
TIMES	: '*' ;
INTEGER : ('-')?(('1'..'9')('0'..'9')*) | '0';
TRUE	: 'true' ;
FALSE	: 'false' ;
LPAR 	: '(' ;
RPAR	: ')' ;
CLPAR 	: '{' ;
CRPAR	: '}' ;
IF 	    : 'if' ;
THEN 	: 'then' ;
ELSE 	: 'else' ;
PRINT	: 'print' ; 
LET	    : 'let' ;
IN	    : 'in' ;
VAR	    : 'var' ;
FUN	    : 'fun' ;
INT	    : 'int' ;
BOOL	: 'bool' ;

LEQ		: '<=' ;
GEQ		: '>=' ;
AND		: '&&' ;
OR		: '||' ;
NOT		: '!' ;
DIV		: '/' ;
MINUS	: '-' ;

ARROW	: '->' ;

CLASS  	: 'class' ; 
EXTENDS : 'extends' ;  
NEW   	: 'new' ;  
NULL    : 'null' ;
 
ID 	    : ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')* ; 

DOT		: '.' ;

WHITESP : (' '|'\t'|'\n'|'\r')+ -> channel(HIDDEN) ;

COMMENT : '/*' (.)*? '*/' -> channel(HIDDEN) ;

ERR     : . { System.out.println("Invalid char: "+ getText()); lexicalErrors++; } -> channel(HIDDEN); 
